<?php

class Bot
{
    private $DB;

    public function __construct($name, $db)
    {
        $this->name = $name;
        $this->DB = $db;
    }

    public function getEnergy()
    {
        $bot = $this->DB->getBot('Razer');
        $row = $bot->fetch_array(MYSQLI_BOTH);
        $energy = $row['energy'];
        return $energy;
    }

    public function getState()
    {
    }

    private function getBotProp($property)
    {
        global $conn;
        $sql = getRs("select '$property' from bots where id='$this->identifier'", $conn);

        $row = $sql->fetch_array(MYSQLI_BOTH);
        return $row[$property];
    }

    public function getAttack()
    {
        $this->getBotProp("attack");
    }

    public function getEvasion()
    {

    }

    public function changeEnergy($newEnergy)
    {
        $this->energy += $newEnergy;
    }

    public function changeState()
    {

    }

    public function checkDeath()
    {

    }

    public function idleReward()
    {
        $this->changeEnergy(2);
    }
}
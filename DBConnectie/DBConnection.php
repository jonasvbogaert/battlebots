<?php

class DBConnection
{
    private $host = "192.168.10.10";
    private $db = "battle_bots";
    private $username = "homestead";
    private $password = "secret";
    private $connection;

    /**
     * DBConnection constructor.
     */
    public function __construct()
    {
        $this->connection = new mysqli($this->host, $this->username, $this->password, $this->db);
        if ($this->connection->connect_error) {
            die("Connection failed: " . $this->connection->connect_error);
        } else {
            return $this->connection;
        }
    }

    /**
     * @param $sql
     * @return bool|mysqli_result
     */
    public function getResult($sql)
    {
        $result = $this->connection->query($sql);
        if ($result === false || $result === null) {
            print $this->connection->connect_error;
        }
        return $result;
    }

    /**
     * @param $name
     * @param int $evasion
     * @param int $attack
     * @param int $energy
     * @param string $state
     */
    public function newBot($name, $evasion = 10, $attack = 10, $energy = 10, $state = "I")
    {
        $state = strtoupper($state);
        $sql = "INSERT INTO bots (name, evasion, attack, energy, state) VALUES ('$name', $evasion, $attack, $energy, '$state')";
        $this->getResult($sql);
    }

    public function getBots()
    {
        $sql = "SELECT name FROM bots";
        $rs = $this->getResult($sql);
        return $rs;
    }

    /**
     * @param $name
     * @return bool|mysqli_result
     */
    public function getBot($name)
    {
        $sql = "SELECT * FROM bots WHERE name = '$name'";
        return $this->getResult($sql);
    }

    /**
     * @param $name
     * @param $energy
     */
    public function setBotEnergy($name, $energy)
    {
        $sql = "UPDATE bots SET energy = $energy WHERE name = '$name'";
        $this->getResult($sql);
    }

    /**
     * @param $name
     * @param $state
     */
    public function setBotState($name, $state)
    {
        $sql = "UPDATE bots SET state = '$state' WHERE name = '$name'";
        $this->getResult($sql);
    }

    /**
     * @param $name
     */
    public function killBot($name)
    {
        $sql = "DELETE FROM bots WHERE name = '$name'";
        $this->getResult($sql);
    }
}
-- --------------------------------------------------------
-- Host:                         192.168.10.10
-- Server version:               5.7.17-0ubuntu0.16.04.2 - (Ubuntu)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for battle_bots
CREATE DATABASE IF NOT EXISTS `battle_bots` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `battle_bots`;

-- Dumping structure for table battle_bots.bots
CREATE TABLE IF NOT EXISTS `bots` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(35) DEFAULT '0',
  `evasion` tinyint(4) DEFAULT '0',
  `attack` tinyint(4) DEFAULT '0',
  `energy` tinyint(4) DEFAULT '0',
  `state` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- Dumping data for table battle_bots.bots: ~16 rows (approximately)
/*!40000 ALTER TABLE `bots` DISABLE KEYS */;
REPLACE INTO `bots` (`id`, `name`, `evasion`, `attack`, `energy`, `state`) VALUES
	(1, 'Razer', 10, 10, 50, NULL),
	(2, 'Minotaur', 10, 10, 30, NULL),
	(4, 'Test', 20, 10, 10, 'i'),
	(5, 'New', 20, 10, 10, 'I'),
	(6, 'New', 20, 10, 10, 'I'),
	(7, 'New', 20, 10, 10, 'I'),
	(8, 'New', 20, 10, 10, 'I'),
	(9, 'New', 20, 10, 10, 'I'),
	(10, 'New', 20, 10, 10, 'I'),
	(11, 'New', 20, 10, 10, 'I'),
	(12, 'New', 20, 10, 10, 'I'),
	(13, 'New', 20, 10, 10, 'I'),
	(14, 'New', 20, 10, 10, 'I'),
	(15, 'New', 20, 10, 10, 'I'),
	(16, 'New', 20, 10, 10, 'I');
/*!40000 ALTER TABLE `bots` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

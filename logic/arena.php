<?php

class Arena
{
    private $bots;
    private $DB;

    public function __construct($db)
    {
        $this->DB = $db;
        $this->bots = $this->DB->getBots();
    }

    public function addBot($bot)
    {
        $this->DB->newBot($bot->name, 10, 10, 10, 100);
        array_push($this->bots, $bot->name);
    }

    public function resolveAttack($attacker, $defender)
    {
        $this->fireIdleReward();

        $a = $attacker . getAttack();
        $ds = $defender . getState();
        $damage = 0;

        if ($ds == 'E') {
            $de = $defender . getEvasion();
            $evasionChange = int(100 / $de);
            $random = rand(0, $evasionChange);

            if ($random != 0) {
                $damage = $a;
            }

            $damage += 2;
        } elseif ($ds == 'A') {
            $damage = 1.25 * $a;

        } elseif ($ds == 'I') {
            $damage = $a;
        }

        $defender . changeEnergy($damage);
        $attacker . changeEnergy(5);

        $defender . checkDeath();
        $attacker . checkDeath();
    }

    public function fireIdleReward()
    {
        foreach ($this->bots as $key => $value) {
            $value . idleReward();
        }
    }


}


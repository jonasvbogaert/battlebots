<?php
/**
 * Created by PhpStorm.
 * User: max
 * Date: 20/06/2017
 * Time: 14:09
 */

class Ui {
    public $arena;
    public $bot;

    public function __construct($bot)
    {
        $this->bot = $bot;
    }

    /**
     * @return mixed
     */
    public function getArena()
    {
        return $this->arena;
    }

    /**
     * @param mixed $arena
     */
    public function setArena($arena)
    {
        $this->arena = $arena;
    }


}
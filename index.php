<?php

require_once 'DBConnectie/DBConnection.php';
require_once 'classes/Bot.php';
require_once 'logic/arena.php';
$db = new DBConnection();
$bot = new Bot('NewBot', $db);
$arena = new Arena($db);
$arena->addBot($bot);
?>

<html>
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="css/arena.css">


</head>
<body>
<nav>
</nav>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div id="1" class="bot"></div>

        </div>
        <div class="col-md-3">
            <div id="2" class="bot"></div>


        </div>
        <div class="col-md-3">
            <div id="3" class="bot"></div>


        </div>
        <div class="col-md-3">
            <div id="4" class="bot"></div>


        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div id="evade"></div>
        </div>


        <div class="col-md-9">
            <div id="energy"></div>


        </div>
    </div>
    <div class="row">

        <div class="col-md-3">
            <div id="idle"></div>
        </div>


    </div>
</div>


<script src="js/bot.js"></script>
</body>
</html>
